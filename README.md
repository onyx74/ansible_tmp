Useful commands:

Get user with name vgladush
curl -X GET --digest --user admin:secret http://34.211.191.1/a/accounts/?suggest\&q=vgladush

Add user 100009 to Administrators group
curl -X PUT --digest --user admin:secret http://34.211.191.1/a/groups/Administrators/members/1000009

List all groups
curl -X GET --digest --user admin:secret http://34.211.191.1/a/groups/

Delete account's name
curl -s --digest --user admin:secret -X DELETE http://34.211.191.1/a/accounts/1000002/name

# Adding user to Administrators group
USERNAME=Gladush
GERRIT_IP=35.164.137.48
PASS=admin:secret
USER_ID=1000002
NAME=Vytalyi%20Gladush

curl -s --digest --user $PASS -X PUT http://$GERRIT_IP/a/groups/Administrators/members/$USER_ID


EMAIL
Thank you for choosing vfemail.net

Your email address is notification.center@offensivelytolerant.com
Your FULL Email address is your username

You now have the following options:

    Change your account settings, including account recovery info.
    Use the web mail
    Configure an email client

    External Email clients, such as Outlook, configure as follows:
    Server: mail.vfemail.net
    POP Port: 110 (or 995 with SSL enabled)
    IMAP Port: 114 (or 993 with SSL enabled)
    SMTP Port: 587 (or 465 with SSL enabled)
    SMTP Authentication MUST be enabled

Please wait 24 hours before registering again.
